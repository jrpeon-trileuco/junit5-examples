package jrpeon.junit5examples;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class SimpleTest {

  @Test
  void encode_givenString_shouldEncodeItWithBasicBase64() {
    String source = "testing base 64";
    String expected = "dGVzdGluZyBiYXNlIDY0";

    String encoded = Base64Utils.encode(source);

    assertThat(encoded).isEqualTo(expected);
  }

}
