package jrpeon.junit5examples;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

class CsvSourceTest {

  @ParameterizedTest
  @CsvSource(value = {
      "amigo:best:-1",
      "back:back:0",
      "back:best:-4",
      "hello:bye:6",
      "1:2:-1"},
      delimiter = ':')
  void token_comparedToOther_shouldFollowStringOrder(String value1, String value2, int expected) {
    assertThat(Token.fromString(value1)
        .compareTo(Token.fromString(value2)))
        .isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/csv-source/plural-forms-for-locale.csv", numLinesToSkip = 1)
  void pluralFormsFor_givenLocale_shouldReturnCorrectForms(String input, String output) {
    Set<PluralForm> expected = Stream.of(output.split(" "))
        .map(PluralForm::fromString)
        .collect(Collectors.toSet());

    Set<PluralForm> actual = LanguageUtils.pluralFormsFor(Locale.forLanguageTag(input));

    assertThat(actual).isEqualTo(expected);
  }


}
