package jrpeon.junit5examples;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;
import jrpeon.junit5examples.exception.InvalidParameterException;
import jrpeon.junit5examples.exception.RequiredParameterException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class ParametersTest {

  @ParameterizedTest
  @ValueSource(strings = {"test-user", "contains spaces", "this_is_too_long_to_be_a_valid_name_and_should_fail"})
  void username_whenValueIsInvalid_shouldThrowException(String value) {
    InvalidParameterException e = assertThrows(InvalidParameterException.class,
        () -> Username.fromString(value));

    assertThat(e.getMessage()).contains("Invalid parameter 'value'").contains("must match ^[\\w]{1,32}$");
  }

  @ParameterizedTest
  @NullAndEmptySource
  void username_whenValueIsNullOrEmpty_shouldThrowException(String value) {
    RequiredParameterException e = assertThrows(RequiredParameterException.class,
        () -> Username.fromString(value));

    assertThat(e.getMessage()).isEqualTo("Parameter 'value' is required");
  }

  @ParameterizedTest
  @MethodSource("blankStrings")
  void username_whenValueIsMissing_shouldThrowException(String value) {
    RequiredParameterException e = assertThrows(RequiredParameterException.class,
        () -> Username.fromString(value));

    assertThat(e.getMessage()).isEqualTo("Parameter 'value' is required");
  }

  static Stream<String> blankStrings() {
    return Stream.of(null, "", "   ");
  }

}
