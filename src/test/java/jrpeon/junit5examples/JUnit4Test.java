package jrpeon.junit5examples;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;
import jrpeon.junit5examples.exception.InstanceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class JUnit4Test {

  private static final ProductId PRODUCT_ID = ProductId.fromLong(1234L);

  @Mock
  private ProductRepository mockRepo;

  private GetProduct getProduct;

  @Before
  public void setUp() {
    getProduct = new GetProduct(mockRepo);
  }

  @Test
  public void getProduct_whenProductExists_shouldReturnIt() {
    Product expected = buildProduct();
    when(mockRepo.find(PRODUCT_ID)).thenReturn(Optional.of(expected));

    Product found = getProduct.execute(PRODUCT_ID);

    assertThat(found).isEqualTo(expected);
  }

  @Test
  public void getProduct_whenProductDoesNotExist_shouldThrowException() {
    when(mockRepo.find(PRODUCT_ID)).thenReturn(Optional.empty());

    InstanceNotFoundException e = Assertions.assertThrows(InstanceNotFoundException.class,
        () -> getProduct.execute(PRODUCT_ID));

    assertThat(e.getMessage()).isEqualTo("'Product' with ID '1234' not found");
  }

  private Product buildProduct() {
    return Product.builder()
        .id(PRODUCT_ID)
        .build();
  }

}
