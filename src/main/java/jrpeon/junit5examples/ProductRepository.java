package jrpeon.junit5examples;

import java.util.Optional;

public interface ProductRepository {

  Optional<Product> find(ProductId id);

}
