package jrpeon.junit5examples;

import java.util.Objects;

public enum PluralForm {
  ZERO,
  ONE,
  TWO,
  FEW,
  MANY,
  OTHER;

  public static PluralForm fromString(String value) {
    Objects.requireNonNull(value);
    return valueOf(value.toUpperCase());
  }

}