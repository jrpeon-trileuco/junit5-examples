package jrpeon.junit5examples;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Base64.Encoder;

public class Base64Utils {

  private static final Encoder BASIC_ENCODER = Base64.getEncoder();

  private Base64Utils() {
  }

  public static String encode(String sourceString) {
    byte[] encodedBytes = BASIC_ENCODER.encode(sourceString.getBytes(StandardCharsets.UTF_8));
    return new String(encodedBytes, StandardCharsets.UTF_8);
  }

}