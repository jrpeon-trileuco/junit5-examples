package jrpeon.junit5examples.exception;

public class InstanceNotFoundException extends RuntimeException {

    private static final String MESSAGE_PATTERN = "'%s' with ID '%s' not found";

    public InstanceNotFoundException(Class<?> entityType, String id) {
      super(String.format("'%s' with ID '%s' not found", entityType.getSimpleName(), id));
    }

}
