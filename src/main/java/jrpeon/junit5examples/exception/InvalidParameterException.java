package jrpeon.junit5examples.exception;

public class InvalidParameterException extends RuntimeException {

  private static final String MESSAGE_PATTERN = "Invalid parameter '%s' with value '%s', %s";

  public InvalidParameterException(String name, String value, String details) {
    super(String.format(MESSAGE_PATTERN, name, value, details));
  }

}
