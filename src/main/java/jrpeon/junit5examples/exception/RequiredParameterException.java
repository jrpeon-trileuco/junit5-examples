package jrpeon.junit5examples.exception;

public class RequiredParameterException extends RuntimeException {

  private static final String MESSAGE_PATTERN = "Parameter '%s' is required";

  public RequiredParameterException(String name) {
    super(String.format(MESSAGE_PATTERN, name));
  }

}
