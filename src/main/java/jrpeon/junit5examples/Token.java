package jrpeon.junit5examples;

import jrpeon.junit5examples.exception.RequiredParameterException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Token implements Comparable<Token> {

  private final String value;

  private Token(String value) {
    if (StringUtils.isBlank(value)) {
      throw new RequiredParameterException("value");
    }
    this.value = value;
  }

  public static Token fromString(String value) {
    return new Token(value);
  }

  public String value() {
    return value;
  }

  @Override
  public int compareTo(Token other) {
    return value.compareTo(other.value);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Token token = (Token) o;
    return new EqualsBuilder().append(value, token.value).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(value).toHashCode();
  }

  @Override
  public String toString() {
    return value;
  }

}
