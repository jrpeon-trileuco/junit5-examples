package jrpeon.junit5examples;

import com.ibm.icu.text.PluralRules;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public final class LanguageUtils {

  private LanguageUtils() {
  }

  public static Set<PluralForm> pluralFormsFor(Locale locale) {
    Objects.requireNonNull(locale);
    return PluralRules.forLocale(locale)
        .getKeywords()
        .stream()
        .map(PluralForm::fromString)
        .collect(Collectors.toSet());
  }

}
