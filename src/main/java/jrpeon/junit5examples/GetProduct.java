package jrpeon.junit5examples;

import java.util.Objects;
import jrpeon.junit5examples.exception.InstanceNotFoundException;

public class GetProduct {

  private final ProductRepository repo;

  public GetProduct(ProductRepository repo) {
    Objects.requireNonNull(repo);
    this.repo = repo;
  }

  public Product execute(ProductId id) {
    return repo.find(id)
        .orElseThrow(() -> new InstanceNotFoundException(Product.class, id.toString()));
  }

}
