package jrpeon.junit5examples;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ProductId {

  private final long value;

  private ProductId(long value) {
    this.value = value;
  }

  public static ProductId fromLong(long value) {
    return new ProductId(value);
  }

  public long value() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductId productId = (ProductId) o;
    return new EqualsBuilder().append(value, productId.value).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(value).toHashCode();
  }

  @Override
  public String toString() {
    return Long.toString(value);
  }
}
