package jrpeon.junit5examples;

import java.util.Objects;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Product {

  private final ProductId id;

  private Product(ProductId id) {
    Objects.requireNonNull(id);
    this.id = id;
  }

  public ProductId getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return new EqualsBuilder().append(id, product.id).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(id).toHashCode();
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .toString();
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {

    private ProductId id;

    private Builder() {
    }

    public Builder id(ProductId id) {
      this.id = id;
      return this;
    }

    public Product build() {
      return new Product(id);
    }
  }
}
