package jrpeon.junit5examples;

import java.util.Objects;
import java.util.regex.Pattern;
import jrpeon.junit5examples.exception.InvalidParameterException;
import jrpeon.junit5examples.exception.RequiredParameterException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Username implements Comparable<Username> {

  private static final Pattern USERNAME_PATTERN = Pattern.compile("^[\\w]{1,32}$");

  private final String value;

  private Username(String value) {
    if (StringUtils.isBlank(value)) {
      throw new RequiredParameterException("value");
    }
    if (!USERNAME_PATTERN.matcher(value).matches()) {
      throw new InvalidParameterException("value", value, "must match " + USERNAME_PATTERN.pattern());
    }
    this.value = value;
  }

  public static Username fromString(String value) {
    return new Username(value);
  }

  public String value() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Username username = (Username) o;
    return new EqualsBuilder().append(value, username.value).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37).append(value).toHashCode();
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public int compareTo(Username other) {
    return value.compareTo(other.value);
  }
}
