# JUnit 5 Examples

A brief collection of example tests that showcase some new features in JUnit 5.

## Getting Started

### Prerequisites

You'll need:

* JDK 11
* Gradle 6

### Running the tests

Compile and run all tests with:
```
gradle build
```
   
Run tests individually with:
```
gradle test --tests "<class_or_method_fqn>"
```

## List of Examples

Each test class in `/src/test/java` contains examples for one or more features:

* `SimpleTest` Minimal example. Package-private methods and classes.
* `ExtendedTest` Mocked dependencies by applying [Extensions](https://junit.org/junit5/docs/snapshot/user-guide/#extensions) (Junit5's generalization of Runners). Test setup with `@BeforeEach`. Exception inspection with `assertThrows`.
* `NestedTest` Nested tests.
* `JUnit4Test` Pure JUnit 4 tests (backwards compatibility)
* `ParametersTest` Parameterized tests.
* `CsvSourceTests` Parameterized tests with CSV inputs.

## Other Interesting Features

* [Additional assertions](https://junit.org/junit5/docs/snapshot/user-guide/#writing-tests-assertions):
   + `assertAll`
   + `assertTimeout`
   
* [Test tags](https://junit.org/junit5/docs/snapshot/user-guide/#writing-tests-tagging-and-filtering)

* [Conditional tests](https://junit.org/junit5/docs/snapshot/user-guide/#writing-tests-conditional-execution)

* [Support for JUnit 4 runners](https://junit.org/junit5/docs/snapshot/user-guide/#running-tests-junit-platform-runner)

* [Parallel execution](https://junit.org/junit5/docs/snapshot/user-guide/#writing-tests-parallel-execution)

## Further Reading

* Brief summary article: https://www.netcentric.biz/insights/2020/07/junit5-new-features.html
* Junit 5 User Guide: https://junit.org/junit5/docs/snapshot/user-guide/#overview
